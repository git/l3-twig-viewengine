<?php

namespace elanpl\L3\TwigViewEngine;

class TwigWrapper implements \elanpl\L3\IViewEngine{

    public $config;
    public $loader;
    public $twig;
    public $twigVersion;
    public $_L3;

    public function __construct($_L3, $config)
    {
        $this->_L3 = $_L3;
        $this->config = new $config();  
    }

    public function render($view, $context){

        if(\class_exists("\Twig\Loader\FilesystemLoader", true)){
            $twig_loader_class = "\Twig\Loader\FilesystemLoader";
            $twig_environment_class = "\Twig\Environment";
            $this->twigVersion = \Twig\Environment::VERSION;
        }
        else{
            $twig_loader_class = "\Twig_Loader_Filesystem";
            $twig_environment_class = "\Twig_Environment";
            $this->twigVersion = \Twig_Environment::VERSION;
        }


        $this->loader = new $twig_loader_class(dirname($view));

        if(isset($this->config)){
            $this->config->configureLoader($this);
        }

        $this->twig = new $twig_environment_class($this->loader, $this->config->getTwigOptions());
        
        if(isset($this->config)){
            $this->config->configure($this);
        } 

        return $this->twig->render(basename($view), $context);
    }

}