<?php

namespace elanpl\L3\TwigViewEngine;

class Config{
    public function configureLoader($twigWrapper){
        // to be overriden
    }

    public function configure($twigWrapper){
        // to be overriden
    }

    public function getTwigOptions(){
        // to be overriden
        return array();
    }
}